import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class Ejercicio3 {
    public static void main(String[] args) throws IOException {
        String sisOp = System.getProperty("os.name").toLowerCase();
        String comando = "";

        //Compruebo el sistema operativo para decidir la ruta
        if(sisOp.equals("windows 10")){
            comando = "java -jar ..\\psppractica2\\out\\artifacts\\minusculas_jar\\psppractica2.jar";
        }else{
            comando = "java -jar ../psppractica2/out/artifacts/minusculas_jar/psppractica2.jar";
        }

        List<String> argsList = new ArrayList<>(Arrays.asList(comando.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(argsList);


        try{
            Process process = pb.start();
            OutputStream os = process.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            Scanner procesoSC = new Scanner(process.getInputStream());

            Scanner sc = new Scanner(System.in);

            String linea = sc.nextLine();
            //Llamo al proceso secundario minusculas toma la linea que escribo por medio del scanner
            //el proceso secundario lo toma y convierte dicha linea a minusculas y la muestra desde el proceso secundario
            while(!linea.equals("finalizar")){
                bw.write(linea);
                bw.newLine();
                bw.flush();
                System.out.println(procesoSC.nextLine());
                linea = sc.nextLine();
            }

        }catch (IOException ex){
            Logger.getLogger(Ejercicio2.class.getName());
        }
    }

}
