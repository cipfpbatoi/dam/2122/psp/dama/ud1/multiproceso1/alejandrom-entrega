import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;


public class Ejercicio1 {
    /**
     * En general el programa principal jamás ha de lanzar una excepción
     */
    public static void main(String[] args) throws IOException,InterruptedException {
        //Si no  se pone ningun argumento falla
        if (args.length < 1) {
            System.err.println("Error, introduce un argumento");
            System.exit(-1);
        }
        ProcessBuilder pb = new ProcessBuilder(args);
        Process process = null;

        /**
         * Aquí puede hacerse uso de pb.inheritIO() y esperar al proceso 
         * mediante pb.waitfor() con lo que podemos ahorrarnos gran parte del código
         */
        try {
            process = pb.start();
            //Si el programa tarda mas de 2 segundos interrumpe el programa y manda la excepción
            if (!process.waitFor(2,TimeUnit.SECONDS)){
                /**
                 * Mejor utilizar otro tipo de herramientas
                 * process.destroy();
                 */
                throw new InterruptedException();
            }
        } catch(IOException ex) {
            System.err.println("Error, comando no encontrado o mal escrito");
            System.exit(-1);
        } catch (InterruptedException ex){
            System.err.println("Tiempo agotado");
            System.exit(-1);
        }
        //Creo el archivo output donde se va a guardar la salida del comando
        File archivo = new File("output.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter(archivo));
        String texto = "";
        try{
            //Leo linea por linea el Stream que devuelve el comando y lo guardo en un String
            InputStream is = process.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            Scanner sc = new Scanner(br);
            /**
             * La manera de escribir es línea por línea, no montar un 
             * String entero porque puede que nos quedemos sin capacidad.
             * 
             * Y además, como has utilizado el BufferedWriter no has de 
             * utilizar el \n porque el salto de línea depende de la plataforma
             * 
             */
            while(sc.hasNext()){
                bw.write(sc.nextLine());
                bw.newLine();
                /**
                 * Opcionalmente, puede realizarse un flush, en
                 * este caso cada linea, pero podría ser cada 5 líneas por ejemplo para que 
                 * sea más eficientem Así en caso de que salta una excepción, 
                 * por lo menos se ha guardado algo y no tenemos penalización en rendimiento.
                 * 
                 * En caso de no usar flush sólo se escribe en el fichero cuando se llega 
                 * al close();
                 * 
                 */
                bw.flush(); 
                
            }
            //Finalmente lo guardo en el fichero
            bw.close(); //Esta sentencia debe ir en un finally
            System.out.println("Proceso ejecutado correctamente, resultado almacenado en *output.txt*");
        }catch (Exception ex){
            System.err.println("Error de proceso hijo");
            System.exit(-1);
        }
    }
}
